from flask import Flask, render_template, redirect, request,send_from_directory
from pymongo import MongoClient
from classes import *

# config system
app = Flask(__name__)
app.config.update(dict(SECRET_KEY='yoursecretkey'))
client = MongoClient('localhost:27017')
db = client.TaskManager
dbCom = client.ComManager

if db.settings.find({'name': 'task_id'}).count() <= 0:
    print("task_id Not found, creating....")
    db.settings.insert_one({'name':'task_id', 'value':0})

if db.settings.find({'name': 'comm_id'}).count() <= 0:
    print("comm_id Not found, creating....")
    db.settings.insert_one({'name':'comm_id', 'value':0})

def updateCommID(value):
    comm_id = db.settings.find_one()['value']
    comm_id += value
    db.settings.update_one(
        {'name':'comm_id'},
        {'$set':
            {'value':comm_id}
        })

def updateTaskID(value):
    task_id = dbCom.find_one()
    task_id += value
    dbCom.update_one(
        {'name':'task_id'},
        {'$set':
            {'value':task_id}
        })

def createTask(form):
    title = form.title.data
    priority = form.priority.data
    shortdesc = form.shortdesc.data
    task_id = db.settings.find_one()['value']
    
    task = {'id':task_id, 'title':title, 'shortdesc':shortdesc, 'priority':priority}

    db.tasks.insert_one(task)
    updateTaskID(1)
    return redirect('/')

def deleteTask(form):
    key = form.key.data
    title = form.title.data

    if(key):
        print(key, type(key))
        db.tasks.delete_many({'id':int(key)})
    else:
        db.tasks.delete_many({'title':title})

    return redirect('/')

def updateTask(form):
    key = form.key.data
    shortdesc = form.shortdesc.data
    
    db.tasks.update_one(
        {"id": int(key)},
        {"$set":
            {"shortdesc": shortdesc}
        }
    )

    return redirect('/')

def resetTask(form):
    db.tasks.drop()
    db.settings.drop()
    db.settings.insert_one({'name':'task_id', 'value':0})
    return redirect('/')

@app.route('/crud', methods=['GET','POST'])
def main():
    # create form
    cform = CreateTask(prefix='cform')
    dform = DeleteTask(prefix='dform')
    uform = UpdateTask(prefix='uform')
    reset = ResetTask(prefix='reset')

    # response
    if cform.validate_on_submit() and cform.create.data:
        return createTask(cform)
    if dform.validate_on_submit() and dform.delete.data:
        return deleteTask(dform)
    if uform.validate_on_submit() and uform.update.data:
        return updateTask(uform)
    if reset.validate_on_submit() and reset.reset.data:
        return resetTask(reset)

    # read all data
    docs = db.tasks.find()
    data = []
    for i in docs:
        data.append(i)

    return render_template('home.html', cform = cform, dform = dform, uform = uform, \
            data = data, reset = reset)

@app.route('/', methods=['GET'])
def home():
    return render_template('home.html')

@app.route('/country/<country>', methods=['GET'])
def country(country):
    # create form
    data = dbCom.settings.find({ 'country': country })
    return render_template('country.html', data=data, cID=country)

    
@app.route('/AddComm', methods=['POST'])
def AddComm():
    country = request.form['country']
    comm_in = request.form['comm_in']
    comm_out = request.form['comm_out']
    date = request.form['date']
    comm_id =dbCom.settings.find_one()   
    comm = {'country':country,'comm_in':comm_in,'comm_out':comm_out, 'date':date}
    dbCom.settings.insert_one(comm)
    return redirect("/country/"+country)

@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

if __name__=='__main__':
    app.run(debug=True)
